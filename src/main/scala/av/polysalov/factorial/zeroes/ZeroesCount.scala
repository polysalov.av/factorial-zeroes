package av.polysalov.factorial.zeroes

import scala.annotation.tailrec

class ZeroesCount extends PartialFunction[Int, Int] {

  override def isDefinedAt(x: Int): Boolean = x >= 0

  override def apply(n: Int): Int = {
    require(n >= 0, "n should be >= 0")

    @tailrec
    def loop(k: Int, acc: Int): Int = {
      if (k <= 0) {
        acc
      } else {
        val l = k / 5
        loop(l, acc + l)
      }
    }

    loop(n, 0)

  }
}
