package av.polysalov.factorial.zeroes

import org.scalatest.FunSuite
import org.scalatest.Matchers._
import org.scalatest.prop.TableDrivenPropertyChecks._

class ZeroesCountTest extends FunSuite {
  val zeroesCount = new ZeroesCount

  test("it should thrown an exception if n < 0") {
    the [IllegalArgumentException] thrownBy zeroesCount(-1) should have message "requirement failed: n should be >= 0"
  }

  test("it should correct count tailing zeroes") {
    val table = Table(
      ("input", "expected"),
      (1, 0),
      (5, 1),
      (6, 1),
      (10, 2),
      (11, 2),
      (15, 3),
      (16, 3)
    )

    forAll(table) { (n, cnt) =>
      zeroesCount(n) shouldBe cnt
    }
  }
}
